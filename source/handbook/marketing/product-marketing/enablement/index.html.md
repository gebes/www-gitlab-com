---
layout: markdown_page
title: "Sales Enablement"
---



## Sales Enablement
### Enablement Setup Instructions 
Sales Enablement is typically livestreamed on Youtube using Google Hangouts (for the first 50 participants).  There are several pre-requisites to successfully host enablement.


#### YouTube - GitLab channel manager access.   
This is required so you can manage the GitLab channel and live stream from the channel as "GitLab".  
If you don't have access, then follow the instructions in OnePass to be added to the YouTube account.  

Here's how to check if you have access:

| Step     | View |
| --------- | ------------ |
| 1. When in Chrome as your name@gitlab.com account, go to Youtube. |    |
| 2. Look at the user icon in the upper right corner, if it isn't the GitLab Tanuki, then you probably need to request access.  | ![YouTube Account Check](/images/handbook/marketing/product-marketing/Youtube-Access.png) |
| 3. Once you have access, click on the user icon and select "Switch Account", then choose the GitLab account.   | ![YouTube Select Account](/images/handbook/marketing/product-marketing/Youtube-Select-Account.png) |
| 4. Here you can see that in YouTube, the GitLab account is now selected.  |  ![YouTube GitLab](/images/handbook/marketing/product-marketing/Youtube-GitLab.png)  |

If you don't have access, then **follow the instructions in OnePass** to be added to the YouTube account. 

#### Set Up Live Stream Event.

| Step     | View |
| --------- | ------------ |
| 1. Once you have access, click on the user icon and select "Switch Account", then choose the GitLab account. | ![YouTube Select Account](/images/handbook/marketing/product-marketing/Youtube-Select-Account.png)  |
| 2.  Here you can see that in YouTube, the GitLab account is now selected.  |  ![YouTube GitLab](/images/handbook/marketing/product-marketing/Youtube-GitLab.png) *This is the right account*  |
| If you're not using the right YouTube account you won't be able to plan and manage live stream events.  Which is critical for you to get the link to the Hangout and to 'Go Live' |   | 
| 3. From here, you can access the 'Creator Studio' |  ![YouTube GitLab](/images/handbook/marketing/product-marketing/Youtube-Creator-Studio.png) *Creator Studio* |
| 4. In Creator Studio, Select the dropdown for Live Streaming-->Events  |  |
|      First this Option |  ![YouTube GitLab](/images/handbook/marketing/product-marketing/Youtube-LiveStream.png)*Live Stream* |
|  Then this option for Events  |  ![YouTube GitLab](/images/handbook/marketing/product-marketing/Youtube-LiveStream-Events.png)*Live Stream-->Events*  |
| 5.  This takes you to the list of the current planned live stream events.  |  ![YouTube GitLab](/images/handbook/marketing/product-marketing/Youtube-Events.png)*Planned Events*  |
|  Your traning enablement MIGHT already exist as a scheduled event.   If it does not, here's how to create a future live stream event: | .  |
|  1. Select New Live Event in the upper right hand part of the page  |  |
|  2. Then fill in the key details: - Title, Start date/time (dont worry about the end time.),  Description (keep the links from the template ). Note: that the event defaults to being public.  When it's all filled in  Click on Create Event  |  ![YouTube GitLab](/images/handbook/marketing/product-marketing/Youtube-CreateEvent.png)*Create new event*  |
|  3. Now the event is added to the list of future events and you can edit Basic Info if you need to.  |  ![YouTube GitLab](/images/handbook/marketing/product-marketing/Youtube-BasicInfo.png)*Basic Info*  |
|  4. From the Basic Info page, you can also navigate to the Event Youtube page "View on Watch Page".  |  ![YouTube GitLab](/images/handbook/marketing/product-marketing/Youtube-BasicInfo-Page.png)*Basic Info Edit Page*  |
|  5. This is the page, where Youtube views can watch live and/or view the event after it's done.  |  ![YouTube GitLab](/images/handbook/marketing/product-marketing/Youtube-ViewEvent.png)*Planned Events*   |

### Team Calendar and Enablement
Team Calendar Edit access (either you need it or you need to know who does) Because we need to update the enablement invitation with the Hangout Link and YouTube links immediately before the enablment starts, either you or some on the team needs to have access to the GitLab team calendar to EDIT and update events.  Currently Ashish, John, Jennifer and ?? have edit access.  see (Team Meetings)[https://about.gitlab.com/handbook/tools-and-tips/#gitlab-team-meetings-calendar]

### Hosting Enablement
How to Host the actual enablement.
**20 minutes before enablement**
1. Go to Youtube, click on the Tanuki icon in the upper right.  Select **"Creator Studio"**
1. Click on **Live Streaming-->Events** in the left menu
1. Find your event from the list.  (if it's not listed, then you need to 'create the event - see above')
1. click on **"Start Hangout On Air"** for your event.   This won't make you go live, but it will get the hangout started, which is where you'll run the event. 
1. Copy the URL from the Hangout, and then go to the **TEAM CALENDAR** event for the session (**NOT YOUR PERSONAL CALENDAR**)... and PASTE the Hangout URL in the event invite. Make sure that there is only one Hangout URL in the invite (the one you are updating). Save this so that attendees can join the hangout.   If you update your personal calendar, then no one will have the new details.   If you don't have edit access, contact Ashish, John, Jenifer, etc to make the update.
1. When it's time to 'go live', click on the go live button.
1. Run the webinar.  Keep an eye on youtube chat, hangouts chat and slack for questions.  It's often best to have a 2nd person help manage q&a.
